import {Module} from "vuex";
import {RootState, GameState, IGame, ICheckResult} from "@/store/types";

const user: Module<GameState, RootState> = {
  namespaced: true,

  state: {
    result: '000',
    key: '',
    checkResult: '',
    waitResult: false,
    waitCheckResult: false
  },
  mutations: {
    ON_RESULT(state, {result, key}: IGame) {
      state.result = result
      state.key = key
      state.waitResult = false
    },
    WAIT_RESULT(state, wait: boolean) {
      state.waitResult = wait
    },
    WAIT_CHECK_RESULT(state, wait: boolean) {
      state.waitCheckResult = wait
    },
    ON_CHECK_RESULT(state, {result}: ICheckResult) {
      state.checkResult = result
      state.waitCheckResult = false
    }
  },
  actions: {
    init({rootState, commit}) {
      rootState.socket.socket.on('game/result', (result: IGame) => commit('ON_RESULT', result))
    },
    start({dispatch, commit}) {
      commit('ON_CHECK_RESULT', {result: ''})
      commit('ON_RESULT', {result: '', key: ''})
      commit('WAIT_RESULT', true)
      return dispatch('request', {url: '/game/start'}, {root: true})
    },
    async check({dispatch, commit, state}) {
      commit('WAIT_CHECK_RESULT', true)
      commit('ON_CHECK_RESULT', await dispatch('request', {
        url: '/game/check',
        data: {gameKey: state.key}
      }, {root: true}))
    }
  }
}
export default user
