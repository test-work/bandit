import Vue from 'vue'
import Vuex from 'vuex'
import game from './game'
import socket from './socket'
import user from './user'
import axios from 'axios'
import {RootState} from "@/store/types";

Vue.use(Vuex)

export default new Vuex.Store({
  state() {
    return {
      inited: false,
      axios: axios.create({
        baseURL: process.env.VUE_APP_SERVER_URI || 'http://localhost:3000',
        responseType: 'json'
      })
    } as RootState
  },
  mutations: {
    INITED(state, inited) {
      state.inited = inited
    },
  },
  actions: {
    async init({dispatch, commit}) {
      await dispatch('user/auth')
      await dispatch('socket/connect')
      await dispatch('game/init')
      commit('INITED', true)
    },
    async request({state}, {url, data = {}, method = 'get'}) {
      return (await state.axios({
        url,
        method,
        [method === 'get' ? 'params' : 'data']: data
      })).data
    }
  },
  modules: {socket, user, game}
})
