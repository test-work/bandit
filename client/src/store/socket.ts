import io from 'socket.io-client';
import {Module} from "vuex";
import {RootState, SocketState} from "@/store/types";

const user: Module<SocketState, RootState> = {
  namespaced: true,

  state: {
    socket: io(process.env.VUE_APP_SERVER_URI || 'http://localhost:3000', {autoConnect: false})
  },
  mutations: {},
  actions: {
    connect({state}) {
      return new Promise((resolve, reject) => {
        state.socket.io.opts.query = {token: localStorage.getItem('token')}

        state.socket.on('connect', function () {
          console.log('socket connect')
          resolve()
        });
        state.socket.on('disconnect', function () {
          console.log('socket disconnect')
        });
        state.socket.on('error', function (error: Error) {
          console.error('sockt error', error)
          reject(error)
        });

        state.socket.connect()
      })
    }
  }
}
export default user
