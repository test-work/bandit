import {AxiosInstance} from 'axios'
import Socket = SocketIOClient.Socket;

export interface RootState {
  inited: boolean
  axios: AxiosInstance
  socket: SocketState
  game: GameState
  user: UserState
}

export interface SocketState {
  socket: Socket
}

export interface GameState {
  waitResult: boolean
  waitCheckResult: boolean
  result: string
  checkResult: string
  key: string
}

export interface UserState {
  authed: boolean
}

export type IGame = {
  result: string
  key: string
}
export type ICheckResult = {
  result: string
}
