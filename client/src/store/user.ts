import {Module} from "vuex";
import {RootState, UserState} from "@/store/types";

const user: Module<UserState, RootState> = {
  namespaced: true,

  state: {
    authed: false
  },
  mutations: {
    AUTHED(state, authed: boolean) {
      state.authed = authed
    }
  },
  actions: {
    async auth({dispatch, rootState, commit}) {
      let token = localStorage.getItem('token')
      if (token) {
        rootState.axios.defaults.headers.auth = token
        try {
          await dispatch('request', {url: '/user/info'}, {root: true})
        } catch (e) {
          console.error(e)
          token = ''
        }
      }
      if (!token) {
        const result = await dispatch('request', {url: '/user/signUp'}, {root: true})
        localStorage.setItem('token', result.token)
        rootState.axios.defaults.headers.auth = result.token
      }
      commit('AUTHED', true)
    }
  }
}
export default user
