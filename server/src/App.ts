import express from 'express'
import Routes from './Routes'
import {Server} from 'socket.io'
import {connectionHandler} from './services/UsersPool'
import cors from 'cors'
import {socketAuthMiddleware} from './middlewares/userAuth'

export default class App {
  expressApp: express.Application

  constructor() {
    this.expressApp = express()
    this.config()
    Routes(this.expressApp)
  }

  config(): void {
    this.expressApp.use(cors())
  }

  initSocket(io: Server): void {
    socketAuthMiddleware(io)
    connectionHandler(io)
  }
}
