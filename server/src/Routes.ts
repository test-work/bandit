import express from 'express'
import {info, signUp} from './controllers/UserController'
import {expressAuthMiddleware} from './middlewares/userAuth'
import {check, start} from './controllers/GameController'

export default function (app: express.Application): void {
  // user routes
  app.get('/user/signUp', signUp)
  app.get('/user/info', expressAuthMiddleware, info)
  // game routes
  app.get('/game/start', expressAuthMiddleware, start)
  app.get('/game/check', expressAuthMiddleware, check)
}
