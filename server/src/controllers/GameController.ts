import {RequestHandler} from 'express'
import {checkGame, doGame} from '../services/Game'
import {emitByUser} from '../services/UsersPool'

/*
 * Start new game
 */
export const start: RequestHandler = async (req, res) => {
  res.json({success: true})
  const game = doGame(req.user.token)
  await new Promise(resolve => setTimeout(resolve, 3000))
  emitByUser(req.user, 'game/result', game)
}

/*
 * Check game result
 */
export const check: RequestHandler = (req, res) => {
  if (typeof req.query.gameKey !== 'string' || req.query.gameKey.trim().length !== 32) {
    res.status(400)
    return res.json({error: {message: 'wrong key format'}})
  }
  res.json({result: checkGame(req.query.gameKey.trim())})
}
