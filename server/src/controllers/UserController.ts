import {RequestHandler} from 'express'
import {generateNewUser} from '../services/UsersPool'

/*
 * Sign up new user
 */
export const signUp: RequestHandler = async (_req, res) => {
  await new Promise(resolve => setTimeout(resolve, 3000))
  res.json({token: generateNewUser().token})
}

/*
 * Get info about current user
 */
export const info: RequestHandler = (req, res) => {
  res.json({
    user: {
      token: req.user.token,
      connections: req.user.sockets.length
    }
  })
}
