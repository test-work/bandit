import {RequestHandler} from 'express'
import {getUser} from '../services/UsersPool'
import {Server} from 'socket.io'
import {UserSocket} from '../types'

export const expressAuthMiddleware: RequestHandler = (req, res, next) => {
  if (!req.headers.auth || !getUser(req.headers.auth as string)) {
    res.status(401)
    return res.json({error: {message: 'Authentication error'}})
  }
  req.user = getUser(req.headers.auth as string)!
  next()
}

export function socketAuthMiddleware(io: Server): void {
  io.use((socket, next) => {
    const query = socket.handshake.query as { token?: string }
    if (query.token && getUser(query.token)) {
      const userSocker = socket as UserSocket
      userSocker.user = getUser(query.token)!
      userSocker.user.sockets.push(userSocker)
      return next()
    }
    return next(new Error('authentication error'))
  })
}
