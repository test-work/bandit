import App from './App'
import http from 'http'
import socket from 'socket.io'



const PORT = process.env.PORT || 3000

const app = new App()

const server = http.createServer(app.expressApp)
app.initSocket(socket(server))

server.listen(PORT, () => {
  console.log(`Api service listening on http://localhost:${PORT}`)
})
