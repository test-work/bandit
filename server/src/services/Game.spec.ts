import {IGame} from '../types'
import {checkGame, doGame} from './Game'

describe('Game result testing', () => {
  let game: IGame
  it('step 1: do game', () => {
    game = doGame('test token')

    // format check key
    expect(game).toHaveProperty('key')
    expect(typeof game.key).toBe('string')
    expect(game.key).toHaveLength(32)

    // format check result
    expect(game).toHaveProperty('result')
    expect(typeof game.result).toBe('string')
    expect(game.result).toHaveLength(3)
  })

  it('step 2: do game check', () => {
    const result = checkGame(game.key)

    // format check result
    expect(typeof result).toBe('string')
    expect(result).toHaveLength(3)

    // check generated result
    expect(result).toBe(game.result)
  })
},)
