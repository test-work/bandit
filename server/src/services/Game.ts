import random from 'random-seed'
import {IGame} from '../types'
import crypto from 'crypto'

export function doGame(userToken: string): IGame {
  const key = crypto.createHash('md5').update(userToken + new Date().getTime().toString()).digest('hex')
  const result = random.create(key).intBetween(0, 999).toString()
  return {
    result: ('00' + result).slice(-3),
    key
  }
}

export function checkGame(key: string): string {
  const result = random.create(key).intBetween(0, 999).toString()
  return ('00' + result).slice(-3)
}
