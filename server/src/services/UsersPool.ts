import crypto from 'crypto'

import {Server} from 'socket.io'
import random from 'random-seed'

import Dict = NodeJS.Dict;
import {IUser, UserSocket} from '../types'

const rand = random.create()

const users: Dict<IUser> = {}

export function emitByUser(user: IUser, event: string, data: unknown): void {
  for (const socket of user.sockets) {
    socket.emit(event, data)
  }
}

export function getUser(token: string): IUser | undefined {
  return users[token]
}

export function generateNewUser(): IUser {
  const key = rand.string(64)
  const token = crypto.createHash('md5').update(key).digest('hex')
  users[token] = {
    token,
    key,
    sockets: []
  }
  return users[token]!
}

export function connectionHandler(io: Server): void {
  io.on('connect', (socket: UserSocket) => {
    console.log(`Connected client socket for: ${socket.user.token} (${socket.user.sockets.length.toString()})`)

    socket.on('disconnect', () => {
      console.log(`Disconnected client socket for: ${socket.user.token} (${socket.user.sockets.length.toString()})`)
    })
  })
}
