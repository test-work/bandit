import {Socket} from 'socket.io'

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    export interface Request {
      user: IUser
    }
  }
}

export interface UserSocket extends Socket {
  user: IUser
}

export type IUser = {
  token: string
  key: string
  sockets: UserSocket[]
}

export type IGame = {
  result: string
  key: string
}
